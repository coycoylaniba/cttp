module.exports = {
  siteMetadata: {
    title: 'Christ to the Philippines',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass'],
}
