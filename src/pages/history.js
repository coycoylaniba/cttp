import React from 'react'

import Layout from '../components/Layout'
import SubHero from '../components/SubBanner'
import History from '../components/History/History'

const HistoryPage = () => {
  return (
    <Layout>
      <SubHero title="History" slug="history" />
      <History />
    </Layout>
  )
}

export default HistoryPage
