import React from 'react'

import Layout from '../components/Layout'
import SubHero from '../components/SubBanner'
import Blog from '../components/Blog/Blog'

const BlogPage = () => {
  return (
    <Layout>
      <SubHero title="Blog" slug="blog" />
      <Blog />
    </Layout>
  )
}

export default BlogPage
