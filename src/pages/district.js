import React from 'react'

import Layout from '../components/Layout'
import SubHero from '../components/SubBanner'
import Leaders from '../components/Leaders'

const DistrictPage = () => {
  return (
    <Layout>
      <SubHero title="District and Leaders" slug="district" />
      <Leaders />
    </Layout>
  )
}

export default DistrictPage
