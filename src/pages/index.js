import React from 'react'

import Layout from '../components/Layout'
import Hero from '../components/Hero'
import Mission from '../components/Mission'
import Vision from '../components/Vision'
import Values from '../components/Values'

const IndexPage = () => {
  return (
    <Layout>
      <Hero />
      <Mission />
      <Vision />
      <Values />
    </Layout>
  )
}

export default IndexPage
