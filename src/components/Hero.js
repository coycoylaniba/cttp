import React from 'react'

import styles from './hero.module.scss'

const Hero = () => {
  return (
    <section className={styles.heroSection}>
      <div className="overlay"></div>
      <div className={`${styles.heroInner} d-flex align-items-center`}>
        <div className="container">
          <div className={`${styles.heroContent} row`}>
            <div className="offset-lg-2 col-lg-8">
              <h1 className="">
                CHRIST TO THE <br />
                <span>PHILIPPINES</span>
              </h1>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Hero
