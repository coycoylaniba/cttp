import React from 'react'

import Profile from './Profile'

import styles from './leaders.module.scss'

const Leaders = () => {
  return (
    <>
      <section className={styles.leadersSection}>
        <div className="container">
          <div className="row align-items-center justify-content-center mb-5">
            <div className="col-lg-12">
              <h1 className={styles.title}>
                THE CHRIST TO THE PHILIPPINES
                <span>7 DISTRICTS</span>
              </h1>
              <p className={styles.description}>
                Be excited for what the Lord is preparing for us these following
                years! For a bigger, brighter, and stronger CTTP!
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-3 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="Rev. Rodrigo G. Sante"
                position="District Director"
                district="District Director"
                img="rodrigo-sante.jpg"
              />
            </div>
            <div className="col-xl-3 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="Rev. Johnny M. Espejo"
                position="District Director"
                district="CTTP-Central Tagalog District"
                img="johnny-espejo.jpg"
              />
            </div>
            <div className="col-xl-3 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="PTR. Vida Louise C. Adela"
                position="District Director"
                district="CTTP-Ilocano District"
                img="vida-louise-adela.jpg"
              />
            </div>
            <div className="col-xl-3 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="PTR. Arlyn C. Miranda"
                position="District Director"
                district="CTTP-North Bicol District"
                img="arlyn-c-miranda.jpg"
              />
            </div>
            <div className="col-xl-4 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="Rev. Antonio Albor"
                position="District Director"
                district="CTTP-South Bicol District"
              />
            </div>
            <div className="col-xl-4 col-md-4 col-sm-6 col-12 mb-5">
              <Profile
                name="Rev. Manolito Tidoy"
                position="District Director"
                district="CTTP-Visayas-Mindanao District"
              />
            </div>
            <div className="col-xl-4 col-md-12 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Miguelito P. PAbilonia, JR."
                position="District Director"
                district="CTTP-Rock District"
                img="miguelito-p-pabilonia-jr-deputy-director.jpg"
              />
            </div>
          </div>
        </div>
      </section>

      <section className={styles.membersSection}>
        <div className="container">
          <div className="row align-items-center justify-content-center mb-3">
            <div className="col-xl-12">
              <h1 className={styles.title}>NATIONAL DIRECTOR</h1>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <Profile name="Ptr. Antonina C. Salonga" opt={true}
                img="antonina-c-salonga-national-director.jpg" />
            </div>
          </div>

          <div className="row align-items-center justify-content-center my-5">
            <div className="col-xl-12">
              <h1 className={styles.title}>NATIONAL BOARD</h1>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. David Camiwet"
                position="Chairman of the Board"
                opt={true}
                img="david-camiwet.jpg"
              />
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Jaime Miranda"
                position="Vice Chairman"
                opt={true}
                img="jaime-miranda.jpg"
              />
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Bien Maniquez"
                position="Corporate Secretary"
                 img="randy.jpg"
                opt={true}
              />
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Danilo De Guzman"
                position="Treasurer"
                opt={true}
                img="danilo-p-de-guzman-corporate-treasurer.jpg"
              />
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Antonina Salonga"
                position="Board Member/Auditor"
                opt={true}
                img="antonina-c-salonga-national-director.jpg"
              />
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-5">
              <Profile
                name="Rev. Jerry Ordiales"
                position="Board Member/Auditor"
                opt={true}
                img="jerry-m-ordiales.jpg"
              />
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Leaders
