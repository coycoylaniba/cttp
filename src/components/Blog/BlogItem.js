import React from 'react'

const BlogItem = props => {
  return (
    <article className="row blog_item">
      <div className="col-md-3">
        <div className="blog_info text-right">
          <div className="post_tag">
            <p>{props.tags}</p>
          </div>
          <ul className="blog_meta list">
            <li>
              <a href="#">
                {props.author}
                <i className="lnr lnr-user"></i>
              </a>
            </li>
            <li>
              <a href="#">
                {props.publishedDate}
                <i className="lnr lnr-calendar-full"></i>
              </a>
            </li>
            <li>
              <a href="#">
                {props.comments}
                <i className="lnr lnr-bubble"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="col-md-9">
        <div className="blog_post">
          <img src="../img/blog/main-blog/m-blog-1.jpg" alt="" />
          <div className="blog_details">
            <a href="#">
              <h2>Astronomy Binoculars A Great Alternative</h2>
            </a>
            <p>
              MCSE boot camps have its supporters and its detractors. Some
              people do not understand why you should have to spend money on
              boot camp when you can get the MCSE study materials yourself at a
              fraction.
            </p>
            <a href="#" className="white_bg_btn">
              View More
            </a>
          </div>
        </div>
      </div>
    </article>
  )
}

export default BlogItem
