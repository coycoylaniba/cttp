import React from 'react'

import styles from './mission.module.scss'

const Mission = () => {
  return (
    <section className={styles.missionSection}>
      <div className="container">
        <div className="row justify-content-center mb-5 text-center">
          <div className="col-lg-12">
            <h1 className={styles.title}>Our Mission</h1>
          </div>
        </div>
        <div className={`${styles.missionBoxContainer} row`}>
          <div className={`col-lg-3 col-md-6 ${styles.missionBox}`}>
            <img src="../../img/icons/home1.png" alt="" />
            <h4>Experience Christ</h4>
          </div>
          <div className={`col-lg-3 col-md-6 ${styles.missionBox}`}>
            <img src="../../img/icons/home2.png" alt="" />
            <h4>Engage People</h4>
          </div>
          <div className={`col-lg-3 col-md-6 ${styles.missionBox}`}>
            <img src="../../img/icons/home3.png" alt="" />
            <h4>Explore Nation</h4>
          </div>
          <div className={`col-lg-3 col-md-6 ${styles.missionBox}`}>
            <img src="../../img/icons/home4.png" alt="" />
            <h4>Expand Church</h4>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Mission
