import React from 'react'
import { Link } from 'gatsby'

import styles from './header.module.scss'

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.navbarWrap}>
        <nav
          className={`${styles.navbar} navbar navbar-expand-lg navbar-light`}
        >
          <div className="container">
            <Link className="navbar-brand logo_h" to="/">
              <img className={styles.logo} src="../../../logo.png" alt="" />
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <div
              className={`collapse navbar-collapse offset ${styles.navbarCollapse}`}
              id="navbarSupportedContent"
            >
              <div className="row ml-0 w-100">
                <div className="col-lg-12 pr-0">
                  <ul
                    className={`nav navbar-nav center_nav pull-right ${styles.nav}`}
                  >
                    <li className={styles.navItem}>
                      <Link
                        className={styles.navLink}
                        to="/"
                        activeClassName={styles.active}
                      >
                        Home
                      </Link>
                    </li>
                    <li className={styles.navItem}>
                      <Link
                        className={styles.navLink}
                        to="/district"
                        activeClassName={styles.active}
                      >
                        District and Leaders
                      </Link>
                    </li>
                    <li className={styles.navItem}>
                      <Link
                        className={styles.navLink}
                        to="/blog"
                        activeClassName={styles.active}
                      >
                        Blog
                      </Link>
                    </li>
                    <li className={styles.navItem}>
                      <Link
                        className={styles.navLink}
                        to="/history"
                        activeClassName={styles.active}
                      >
                        History
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
  )
}

export default Header
