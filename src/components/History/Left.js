import React from 'react'

import styles from './content.module.scss'

const Left = ({ title, children }) => {
  return (
    <div className={`row ${styles.history}`}>
      <div className="col-lg-4 col-12">
        <img src="placeholder.png" alt="placeholder" className="img-fluid" />
      </div>

      <div className="col-lg-8 col-12">
        <h2>{title}</h2>
        {children}
      </div>
    </div>
  )
}

export default Left
