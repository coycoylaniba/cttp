import React from 'react'

import styles from './content.module.scss'

const Right = ({ title, children }) => {
  return (
    <div className={`row ${styles.history}`}>
      <div className={`col-lg-4 col-12 ${styles.imgShowSm}`}>
        <img src="placeholder.png" alt="placeholder" className="img-fluid" />
      </div>

      <div className="col-lg-8 col-12 text-right">
        <h2>{title}</h2>
        {children}
      </div>

      <div className={`col-lg-4 col-12 ${styles.imgShowLg}`}>
        <img src="placeholder.png" alt="placeholder" className="img-fluid" />
      </div>
    </div>
  )
}

export default Right
