import React from 'react'

import Left from './Left'
import Right from './Right'

const History = () => {
  return (
    <div className="container py-5">
      <Left title="The Story">
        <p>
          The story of Christ to the Philippines started with a vision, a vision
          of islands – “The isles shall wait for his law” (Isaiah 42:4). It
          continued with another vision, “the isles shall wait upon me and on my
          arms shall they trust” (Isaiah 51:5). Now we declare his praise in the
          islands” (Isaiah 42:12). These ‘islands’ of course were believed to be
          the Philippine archipelago.
        </p>
      </Left>
      <Right title="The Setting">
        <p>
          Christ to the Philippines came into being some 60 years after the
          first protestant church was established in the Philippines. It was
          during those years when then Ferdinand Marcos was about to finish his
          four-year term as president of the Philippines."
        </p>
      </Right>
      <Left title="A Confirmation from the Lord">
        <p>
          In 1966, Harold Mcdougal Jr., from Fairmount West Virginia, came to
          the Philippines with his wife Diane to proclaim the Gospel of Christ
          to the Philippines with the intention of planting churches.
          Originally, they had in mind going to India to serve as missionaries
          there, but on their way to the country and in transitory ministry in
          Hongkong they were given two tickets for Manila which they
          acknowledged as a confirmation from the Lord to come to the
          Philippines.
        </p>
        <p>
          Rev. Robert Robinson and wife Rita later joined them. He became a
          co-founder of Christ to the Philippines and served as director of CTTP
          from 1973 to 1978. Other American missionaries followed them. They
          conducted evangelistic crusades in many towns, cities and even remote
          villages, with the purpose of planting churches.
        </p>
        <p>
          Along the years, God raised people to support the Mission, Sis Ruth
          Bloom and husband George Hurd(deceased) served as Treasurer of
          Philippine Mission and raised finances for the ministry.
        </p>
      </Left>
      <Right title="The Birth of an Organization">
        <p>
          In 1969, CTTP was formally registered as a non-stock, non-profit
          organization, a denomination aimed at uniting local congregations it
          has established in a single legal administrative body. Training
          workers became an integral part of the ministry using the Short Term
          Training Program.
        </p>
      </Right>
      <Left title="Taking Root in the Heart of the City">
        <p>
          By 1970 Murphy Church found a permanent location for worship. In its
          beginnings, the church had to meet in a rented apartment before
          finally finding a house at 11th Avenue, Murphy Quezon City with an
          ample space at the back where at first, services were conducted under
          some trees. Today a new building is situated to host the worship
          services.
        </p>
      </Left>
      <Right title="Catalyst of the Holy Spirit">
        <p>
          We could very well say that CTTP has played a role in ushering the
          movement of the Holy Spirit among the Catholics in the Philippines. By
          the later part of the 80’s the Catholic Church in the Philippines
          recognized the emergence of Pentecostal and Born Again groups.
        </p>
      </Right>
      <Left title="The Mustard Seed Planted in the City">
        <p>
          CTTP began small and with not much resources but the passion of the
          mostly young and untrained ministers sufficed – a CTTP trait that has
          been noted even by other denominational leaders.
        </p>
        <p>
          In 1976, a daughter missions organization was established in Thailand
          called Christ to Thailand Mission. The year marked the turn for
          Filipinos – Luciano Cariaga, Pedro Belardo, Mario Taculod and Sonny
          Largado – to become founders of a mission group. It would later become
          an umbrella organization for various Christian ministries. Seven years
          later the Mission established a Training Center in Khonkaen located in
          Northeast Thailand.
        </p>
        <p>
          1978 saw the turnover of leadership from the foreign founding
          missionaries to the locals giving birth to all-Filipino CTTP Board,
          with Joey Tupe, former missionary to Vietnam, as director. They
          missionary founders by then believed that it was high time to give
          full authority to the nationals but they were neither independent of
          the Philippine Mission nor continue to be fully dependent on them. The
          aim was to have an interdependent relationship between the two.
        </p>
        <p>
          In 1979, the CTTP Training Center in Murphy was transferred to
          Sampaloc, Tanay, Rizal. It also got a new name, Asian Hope Bible
          School, under the leadership of Levita Dacayanan, former missionary to
          Hongkong. Other Bible Schools were established to serve the districts
          of North and South Bicol.
        </p>
        <p>
          In Thailand, a church called Namphrathai (God’s Will) was established
          in Bangkok under the leadership of Luciano Cariaga. Then a few years
          lalter, Estelita Besas took over and has now served in that capacity
          for ten years.
        </p>
        <p>
          In 1981, Berth Colosaga, former missionary to Ecuador, was elected as
          CTTP Director. A three-year program of church growth was launched. The
          plan was designed to produce simultaneous growth in membership of
          local churches as well as the number of churches under CTTP.
        </p>
        <p>
          Over the years, CTTP has been declaring God’s Word and touching the
          world. As we’ve grown spiritually, we have also grown numerically. To
          date, we have planted 91 churches geographically located in six
          districts all over the country with 150 licensed ministers all
          actively and faithfully serving in God’s Vineyard.
        </p>
      </Left>
    </div>
  )
}

export default History
