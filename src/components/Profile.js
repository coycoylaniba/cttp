import React from 'react'

import styles from './profile.module.scss'

const Profile = ({ name, position, district, opt, img }) => {
  return (
    <div className={styles.profile}>
      <img src={img ? `leaders/${img}` : 'logo.png'} alt="" />
      <h4 className={opt ? styles.white : null}>{name}</h4>
      {position ? (
        <p className={opt ? styles.white : null}>{position}</p>
      ) : null}
      {district ? (
        <p className={opt ? styles.white : null}>{district}</p>
      ) : null}
    </div>
  )
}

export default Profile
