import React from 'react'

import styles from './banner.module.scss'

const Banner = () => {
  return (
    <section className={styles.bannerSection}>
      <div className="overlay"></div>
      <div className={`${styles.bannerInner} d-flex align-items-center`}>
        <div className="container">
          <div className={`${styles.bannerContent} row`}>
            <div className="col-lg-12">
              <span className={styles.text}>CORPORATELY</span>
              <h1 className="">
                GIVING THANKS TO GOD
                <span>FOR HIS FAITHFULNESS IN OUR ORGANIZATION</span>
              </h1>
              <a className={styles.button} href="#">
                JOIN US
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Banner
