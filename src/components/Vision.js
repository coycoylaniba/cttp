import React from 'react'

import styles from './vision.module.scss'

const Vision = () => {
  return (
    <section className={styles.visionSection}>
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-lg-12">
            <h1 className={styles.title}>Our Vision</h1>
            <p className={styles.description}>
              A growing, multiplying and disciple-making church reaching out to
              the Philippines and to the nations for God’s glory.
            </p>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Vision
