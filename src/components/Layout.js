import React from 'react'

import { Helmet } from 'react-helmet'

import Header from './Header/Header'
import Banner from './Banner'

const Layout = props => {
  return (
    <>
      <Helmet>
        <title>Christ to the Philippines</title>
        <link rel="icon" type="image/png" href="logo.png" />
      </Helmet>
      <Header />
      {props.children}
      <Banner />
    </>
  )
}

export default Layout
