import React from 'react'

import styles from './values.module.scss'

const Values = () => {
  return (
    <section className={styles.valuesSection}>
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-lg-12">
            <h1 className={styles.title}>Core Values</h1>
            <div className={styles.description}>
              <div className="row">
                <div className="col-lg-6">
                  <p>
                    <span>D</span>iscipline
                  </p>
                  <p>
                    <span>I</span>ntegrity
                  </p>
                  <p>
                    <span>S</span>ubmission
                  </p>
                  <p>
                    <span>C</span>ommitment
                  </p>
                  <p>
                    <span>I</span>nitiative
                  </p>
                </div>
                <div className="col-lg-6">
                  <p>
                    <span>P</span>artnership
                  </p>
                  <p>
                    <span>L</span>oyalty
                  </p>
                  <p>
                    <span>E</span>xcellence
                  </p>
                  <p>
                    <span>S</span>tewardship
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Values
