import React from 'react'
import { Link } from 'gatsby'

import styles from './subbanner.module.scss'

const SubBanner = props => {
  return (
    <section className={styles.bannerArea}>
      <div className={`${styles.bannerInner} d-flex align-items-center`}>
        <div className="overlay"></div>
        <div className="container">
          <div className={`${styles.bannerContent} text-center`}>
            <h2>{props.title}</h2>
            <div className={styles.pageLink}>
              <Link to="/">Home</Link>
              <Link to={`/${props.slug}`}>{props.title}</Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default SubBanner
